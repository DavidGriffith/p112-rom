	TITLE		"D-X Designs P112 Boot ROM"
	SUBTITLE	"Cold-Start Functions"
;=======================================================================;
;	  C O L D  -  S T A R T   F U N C T I O N S			;
;=======================================================================;
;=======================================================================;
;   D-X Designs Pty. Ltd.   Perth, Western Australia			;
;   Project: 112                       May, 1996			;
;									;
;  Copyright (C) 1996  David R. Brooks					;
;									;
;  This program is free software; you can redistribute it and/or	;
;  modify it under the terms of the GNU General Public License		;
;  as published by the Free Software Foundation; either version 2	;
;  of the License, or (at your option) any later version.		;
;									;
;  This program is distributed in the hope that it will be useful,	;
;  but WITHOUT ANY WARRANTY; without even the implied warranty of	;
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	;
;  GNU General Public License for more details.				;
;									;
;  You should have received a copy of the GNU General Public License	;
;  along with this program; if not, write to the Free Software		;
;  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		;
;									;
; Revision History:							;
;									:
; 13 Dec 2016	Version 5.8 derived from 5.7 below		    WBW	;
;		- Support Z182 clock multiplier.  The clock		;
;		  multiplier bit is in the CMR register which is only	;
;		  available on Z8S180/L180-Class Processors.  The	;
;		  Z80182 incorporated this register starting around	;
;		  1998.  Setting the clock speed to "double" will	;
;		  have no effect on CPU's that do not support the CMR	;
;		  register and will result in normal (full speed)	;
;		  operation.  No attempt is made to determine if the	;
;		  CPU actually supports the CMR.  Bit 7 of nvRam10	;
;		  is used to save setting of the clock multiplier.	;
;									;
;		- Recognize new clock speeds:				;
;			11.0592, 22.1184, 29.4912, 32.000, 36.864,	;
;			44.2368, 49.152, and 58.9824 MHz		;
;		  Note that the timer frequency has been changed from	;
;		  25Hz to 50Hz to avoid overflowing the reload		;
;		  counters.  Timing loops derived from the timer	;
;		  frequency have been updated.				;
;									;
;		- Updated timing loops in diskop.s to support faster	;
;		  clock speeds.						;
;									;
;		- Fixed breakpoint handling in debugger.  Corrected	;
;		  a typo that was introduced in 5.0 --> 5.1.		;
;									:
;		- Fixed wait state insertion setup.  Previously, it	;
;		  was not set to nvRAM values unless the boot option	;
;		  timed out.						;
;									;
;		- The 4-Port LAN overrun condition discussed below is	;
;		  resolved with an update to the WIX140SR firmware.	;
;		  Firmware V1.6.4 is current (as of 10 Jan 17) and the 	;
;		  only recommended firmware. Only Config V1.7 should be	;
;		  used.  Baud rates up to the max of 230,400 baud op-	;
;		  erate flawlessly. HW Flow Control on the WIZ140SR	;
;		  should be set to CTS/RTS and Data Mode to RAW. For	;
;		  earlier firmware versions, Flow Control should be set	;
;		  to NONE.			;
;									:
; 01 Oct 2014	Version 5.7 derived from 5.6 below		    TAG	;
;		- The 4-Port adapter-to-LAN connection has been seen to	;
;		  miss terminal characters coming from the P112.  It	;
;		  appears to be an overrun condition, but the WIZ140's	;
;		  CTS/RTS flow control does not prevent the overrun.	;
;		  LAN traffic also seems to have an impact on the fre-	;
;		  quency of overrun errors.  The Version 5.6 mod added	;
;		  a timing delay between characters but left the baud	;
;		  rates at either 115,200 or 230,400.  The overrun	;
;		  condition was	still present, although the frequency	;
;		  was much reduced and appeared to be more a function	;
;		  of LAN traffic.  The v5.7 mod adds baud rates of	;
;		  38,400 and 57,600 to the 4-Port adapter driver. The	;
;		  default baud rate is set to 38,400.			;
;									;
;		- Additional testing of the 140SR flow control issue	;
;		  seems to indicate that disabling the internal flow	;
;		  control provides much better results.			;
;									;
;		- The inter-character delay added by v5.6 was modified	;
;		  to a very short settle delay rather than a much	;
;		  longer character timing delay.			;
;									;
;		- While making the above modifications, it was noted	;
;		  that the 4-Port baud rate cannot be set unless the	;
;		  Alternate Terminal is first selected.  To set the	;
;		  4-Port baud rate and still remain connected to the	;
;		  Local (Serial) Terminal, use the following sequence:	;
;		    - Enter Setup and select the Alternate Terminal	;
;		    - Select the desired 4-Port Baud Rate		;
;		    - Select the Primary (Serial) Terminal		;
;		    - Exit Setup via ESC to write NvRAM			;
;		  Although this process works, it is cumbersome.  A	;
;		  future code modification will allow you to select the	;
;		  4-Port baud rate separately.				;
;									;
; 7 Aug 2013	Version 5.6 derived from 5.5 below		    TAG ;
;		- There are occasional loss of character sync issues	;
;		  with the 4-Port LAN adapter.  It appears as though	;
;		  the built-in CTS/RTS hardware flow control fails to	;
;		  throttle the character transmission such that the	;
;		  WIZ140SR's character input-output buffer loses sync	;
;		  or suffers overrun.  This modification inserts a	;
;		  slight timing delay between characters when using the	;
;		  LAN Terminal.  It does not affect the serial terminal	;
;		  in any way.						;
;									;
; 11 Nov 2013	Version 5.5 derived from 5.4 below.		    TAG	;
;		- Some CF modules require the full 30 seconds to	;
;		  initialize, but they will work fine with a Status	;
;		  register read prior to the normal 1st access at the	;
;		  start of the CkGIDE routine. A dummy read of the	;
;		  status register overrides this odd behaviour.		;
;		- Some TELNET software sends a two-characeter sequence	;
;		  with every keystroke.  The second character is a NULL	;
;		  that the EEPROM code interprets as a normal char-	;
;		  acter, causing trouble with character entry.  This	;
;		  mod drops NULL characters from the input stream.	;
;		- An IDE drive can take up to 5-7 seconds to come up	;
;		  speed.  If a 'Z 3' boot attempt is made before the	;
;		  drive reports 'ready' the boot will fail with an	;
;		  'Invalid command code' error.  A subsequent 'Z 3'	;
;		  command will work fine.  This modification causes ~7	;
;		  seconds worth of retry of the 'Z 3' command before it	;
;		  declares a fault.					;
;									;
; 08 Oct 2013	Version 5.4 derived from v5.3 below.		    TAG	;
;		- Added code to allow use of LAN terminal without the	;
;		  need to switch to it from the Serial terminal.	;
;								;
; 29 Aug 2013	New Rev 5.3 derived from Rev 5.2 below.		    TAG	;
;		- Bug fix to correct a problem with the terminal	;
;		  redirector flag during boot loader execution.	Bug was	;
;		  only apparent when using LAN-based terminal I/O.	;
;		- Added bit definition to byte nvRam10.  Bit 2 (set)	;
;		  is used to flag the 4-Port UART present. If the UART	;
;		  is not present, the setup menu will not offer to re-	;
;		  direct terminal I/O.					;
;		- Added bit definition to byte nvRam10.  Bit 3 (set)	;
;		  is used to select 230,400 baud; (reset) selects	;
;		  115,200 baud (for Quad UART only.)			;
;		- Added bit definition to byte nvRam10.  Bits 5,4 com-	;
;		  bine to form a 4-option selection for autoboot:	;
;									;
;			00 - No AutoBoot				;
;			01 - AutoBoot chain starting @ Floppy		;
;			10 - AutoBoot chain starting @ SCSI		;
;			11 - AutoBoot chain starting @ GIDE		;
;									;
;  25 Nov 2012	New version 5.2 derived from v5.1 below.	    TAG	;
;		- Prepare for new P112 production run.  Code converted	;
;		  to compile via Zilog Developer Studio (ZDS) 3.68.	;
;		- Using ClkRam byte 10, bit 1 to reflect terminal I/O	;
;		  select. An alternate terminal port can be defined	;
;		  for use with a serial-to-LAN adapter.  Code used for	;
;		  alternate port is based on 16550 port hardware.	;
;		- Alternate Terminal port select is passed to OS via	;
;		  the 4-Port adapter, Port B scratch register.		; 
;		- Modified Memory Sizing routine to address specific	;
;		  requirements of ZDS compiler. The mods amount to	;
;		  removing a MACRO and altering the .PHASE and .DEPHASE	;
;		  code area (ZDS 3.68 does not understand the PHASE/	;
;		  DEPHASE pseudo-ops.)					;
;									;
;  04 Feb 2006	- Merged V5.0 changes (see below). New version V5.1  HP	;
;  03 Nov 2005	- Fix to add DTR enable to ESCC ports, and to add	;
;		  code to enable RTS @ Parallel Port A, Bit 7 (PA7)	;
;		  to correct lack of RTS signal assertion. Enables	;
;		  Hardware Flow Control on the Terminal port.		;
;		- Mod to NV-RAM table - Picked up byte 10 for system	;
;		  mode flag bits.					;
;		- Repaired bug in boot code that was flagging invalid	;
;		  NvRam data to the BIOS as valid.			;
;		- Changed DS-1202 code references to DS-1303 for new	;
;		  system issue.						;
;		- Added 'Load and Return' command to allow loading	;
;		  the 'new' style disk OS and return to the debugger.	;
;		- Upped ROM code base to Version 5.0.  Released.    TAG	;
;									;
;  14 Dec 2004	- Z80182 register setup now done in the correct		;
;		  order.					     HP	;
;									;
;  16 Jan 2004	- When booting from GIDE, check also for a standard	;
;		  boot sector.					     HP	;
;		- Corrected a bug in RTime: mon12 was missing from	;
;		  monTbl					     HP	;
;									;
;  25 Sep 1999	- RAM scan preserves original RAM contents	     HP	;
;									;
;  12 Jul 1997	- Cosmetic fixes per Paul De Bak.  Manually expand	;
;		  CONFIG macro for ZMAC assembly.  Released	    HFB	;
;   4 Jul 1997	- Added defaults to Setup, added Date/Time set		;
;		  for DS-1202, lengthened safety delay time.	    HFB	;
;   3 Jun 1997	- Added Dave Brooks' 06-02-97 and 08-03-97 updates,	;
;		  Boot from GIDE, configuration of NVRam.	    HFB	;
;   4 Aug 96	- Add NV-RAM config for CPU Speed, Mem&IO Waits.	;
;		  Check for DS-1202 presence.			    HFB	;
;  21 Jul 96	- Check DS-1202 RAM Checksum, if valid, Set SCCA	;
;		  Data Rate from RAM0 Configuration byte.	    HFB	;
;   7 Jul 96	- Display Version Number, Correct RAM Scan.	    DRB	;
;=======================================================================;
;  Basic hardware initialisations for the Z80182 CP/M board.
; Determine hardware configuration, and make optimal hardware
; setups. Link with the standard D-X Designs debugger.

	DEFINE	MainRom, ORG=0
	DEFINE	RamCheck, ORG=8200h

	SEGMENT	MainRom

FALSE	EQU	0
TRUE	EQU	!FALSE

DATE	 MACRO
	DB	"13 Dec 2016",0
	 ENDMAC

Version	 MACRO
	db	"5.8"
	 ENDMAC

DBUG		equ	FALSE
TinyBasic	equ	TRUE

Z80182	EQU	1

	INCLUDE	"Z180.S"
	INCLUDE	"EQU.S"		; Get Defined Constant Values
	INCLUDE	"MACROS.S"	; Macros needed in this code
	INCLUDE	"DATA.S"	; DSEG Data locations as "Equates"
;-----------------------------------------------------------------------
;	H O O K S   F O R   B O O T - S E C T O R S ,   E T C .

	  IF  !DBUG
	ORG   0
	  ENDIF

	JP	Begin		; Cold-start entry

	DB	16H, 12H, 13H	; Date code: YYMMDD

	  IF  !DBUG
	 ORG   08H
DiskV:	JP	DiskOp		; Hook to disk routine for boot-loader
	  ENDIF
dpb3:	DW	DPB3i		; Pointers to standard DPB's
dpb5:	DW	DPB5i

	  IF  !DBUG
	ORG	10H
	JP	GetChr		; Get keyboard to A (or Z)

	ORG	18H
	JP	PutChr		; Send A to display

	ORG	20H
	JP	PutMsg		; Display ASCII string

;------------------------------------------------------------------
	ORG   38H
	JP	Rst38		; Trap for debugger
	  ENDIF		;~Dbug
;------------------------------------------------------------------
				; Signature in the ROM code (see also "today")
isdate:	DB	"ROM v"
	Version
	db	" Dated: "
	DATE
	DB	"D-X Designs Pty Ltd, Perth Australia",0

;==================================================================
;	CTC INTERRUPT-SOURCE ROUTINES (for the Debugger)
;
;  The name is a misnomer. These routines are usually supported by
; a Z80-CTC device, however the Z182 does not have one. They are
; therefore supported by Channel-0 of the Programmable Reload Timer
;==================================================================
; Return Address of CTC Interrupt Vector
 
GetVect: LD	A,I		; Read Interrupt Register
	LD	H,A		; MSB - Interrupt page
	IN0	A,(IL)
	ADD	A,VPRT0		; Offset for timer
	LD	L,A		; HL -> Vector locn.
	LD	A,H
	OR	L		; Z set if none. (May corrupt A)
	RET			; Return CTC interrupt vector

;.....
; Trigger an Interrupt ASAP

ArmCTC:	DI
	LD	HL,1		; Get a very short time period
	OUT0	(RLDR0H),H
	OUT0	(RLDR0L),L	; Reload reg.
	OUT0	(TMDR0H),H
	OUT0	(TMDR0L),L	; Down counter
	IN0	A,(TCR)
	OR	11H		; Set timer & interrupt
	OUT0	(TCR),A		;  Trigger the timer
	RET			;   Return with ints. disabled

;.....
; Acknowledge & Cancel Interrupt
;  To Stop the Timer safely, do these EXACTLY as given!!

StopCTC: IN0	A,(TCR)		; Read status first
	AND	0A2H		; Kill Ch-0, don't touch Ch-1
	OUT0	(TCR),A		;  Stop it running
	NOP
	IN0	A,(TCR)		;   Acknowledge the interrupt
	IN0	L,(TMDR0L)	; Must read these, to clear ints.
	IN0	H,(TMDR0H)	;  (they are discarded)
	EI
	RET			; Don't use RETI for internals

;.....
; Basic CTC interrupt initialisation

CTCSetup:
	RET			; Nothing needed for timer

;==================================================================
;	    B A S I C    I N I T I A L I S A T I O N S
;==================================================================

hwMsg:	DB	"Special P112 Version: Interrupts Supported", cr,0

Begin:	DI			; Disable interrupts in case we arrived here
				;  by a software reset
	LD	HL,inzTab	; Basic setups
	XOR	A		; Get a Zero
	LD	B,A		;  for Hi-Order Port #
	CPL			;   Set A = FF
Ini0:	LD	C,(HL)		; Fetch Port #
	TST	C		; End?
	JR	Z,Ini1		; ..jump if So
	INC	HL		; Else pt to data
	OUTI			;  send & Bump Ptr
	INC	B		;   (correct Hi-Adr in B)
	JR	Ini0		;  ..and keep going

Ini1:
; Mod for v5.8
; Ensure most conservative clock speed for now
; These are the same as CPU hard reset defaults
	XOR	A		; Clear Accum
	OUT0	(CCR),A		; Half speed operation
	OUT0	(CMR),A		; ... w/ no multiplier
; End of v5.8 mod

; This is the ESCC port used for Terminal I/O
; Mod for v5.2
	xor	a
	ld	(SysParm),a	; Set flags for Primary terminal, no HFC
; End of v5.2 mod

	LD	HL,iTab		; Set up the Z182 ESCC-A Serial port
	LD	B,(HL)		; # of table entries
Iz1:	INC	HL		; Can't use OTIR: high addr. bits!
	LD	A,(HL)
	OUT0	(SCCACNT),A	; Send it
	DJNZ	Iz1

; Enable the RTS signal on PA7 for the ESCC Terminal Port.	TAG
	IN0	A,(DRA)		; Read the PA data reg
	RES	PaRTS,A		; Enable RTS bit
	OUT0	(DRA),A		;  and set the port

	LD	HL,iTab		;- Set up the ESCC-B Serial port
	LD	B,(HL)		;-
Iz2:	INC	HL		;-
	LD	A,(HL)		;-
	OUT0	(SCCBCNT),A	;-  Send the byte
	DJNZ	Iz2		;-

	  IF  !DBUG
	ld	iy,MemSetRtn
	jp	MemSetup
;	XCall	IY,MemSetup	; Set up the RAM, define stack
MemSetRtn:
	LD	A,HIGH IVPage
	LD	I,A		; Set interrupt page-base
	IM	2		; Interrupt hardware operating mode

	LD	HL,0FFFFH	; Clear out RAM first
	XOR	A
Clp:	LD	(HL),A
	DEC	HL
	BIT	7,H
	JR	NZ,Clp		; Down to 8000H
	  ENDIF		;~Dbug

	LD	SP,0000H	; Put stack at the top
	CALL	NVChk		;; Check DS-1302 presence
	JR	NZ,BadNV	;; ..jump if Not Present
	CALL	NVChkS		;; Is Ram CheckSum Valid?
	JR	NZ,BadNV	;; ..jump if Invalid
	LD	HL,ramAry+10	;; Else Point into 24-byte Array
	LD	A,(HL)
	LD	(nvRam10),A	;; Set SysOP flags
	ld	(SysParm),a
	DEC	HL
	LD	A,(HL)		;;
	LD	(hdSPT),A	;;  Copy
	DEC	HL		;;
	LD	A,(HL)		;;
	LD	(hdHds),A	;;   selected
	DEC	HL		;;
	LD	D,(HL)		;;
	DEC	HL		;;
	LD	E,(HL)		;;
	LD	(hdTrks),DE	;;    values
	DEC	HL		;;
	LD	A,(HL)		;;
	LD	(nvRam5),A	;; (HD Type)
	DEC	HL		;;
	DEC	HL		;;
	DEC	HL		;;
	LD	A,(HL)		;;
	LD	(nvRam2),A	;; (Floppy0 timings)
	DEC	HL		;;
	LD	A,(HL)		;;
	LD	(nvRam1),A	;; (Floppy0 Specs)
	DEC	HL		;;
	LD	A,(HL)		;;
	LD	(nvRam0),A	;; (Speeds)
	AND	00000111B	;; Strip off all but Rate
	JR	OkNV		;; ..continue to save results

BadNV:	OR	0FFH		;; Set "Bad Checksum" Flag
OkNV:	LD	(NVRate),A	;;  save 0FFH if Bad, 00..07H if Good (Rate)
	BIT	7,A		;; Valid?
	JR	NZ,UsDflt	;; ..jump if Not

;; Clock RAM is Valid.  Use stored values to set system parameters
	LD	A,(nvRam0)	;; Get RAM0
	AND	10000000B	;; Get CPU Clock Divisor Bit
	OUT0	(CCR),A		;;  set Hi/Lo
	LD	A,(nvRam10)	;; Get RAM10
	AND	10000000B	;; Get Clock Multiplier Bit
	OUT0	(CMR),A		;;  set Hi/Lo
	JR	UsDfl0		;; ..rejoin main flow

; Clock RAM is Invalid.  Use defaults to set system parameters
UsDflt:
	xor	a
	ld	(ramAry+10),a	; Flag to disable autoboot
; Mod for v5.8
; Updated to apply multiple settings
	LD	HL,inzTb1	; Settings for default proc speed
	CALL	IoInz		; Apply settings
; End of v5.8 mod

; Back to common code
UsDfl0:
	CALL	ClockSpd	; Setup clock and serial ports,
	DB	0		;  but no output yet

	CALL	CTCSetup	; Setup interrupt for debugger

; v5.2 mod
	call	Port4Ini	; Init the 4-Port LAN adapter
; End of v5.2 mod

; v5.4 mod.
;  This modification allows the user to power up the P112 and immediately
; use the LAN Terminal without having to use the Serial terminal for the
; initial setup.  This only applies when the NvRam data is INVALID.  Once
; the NvRam setup is stored, the stored terminal selection is used.

; Test for NvRam not valid
	ld	a,(NVRate)		; Get NvRam status
	bit	7,a			; Test for Valid/Invalid
	jr	z,NvRamValid		;  and go if valid

; NvRam is invalid.  See if LAN Terminal is present - the flag was set
; by the previous call to Port4Ini.

	ld	a,(SysParm)		; Get LAN adapter status
	and	Port4Pres		;  and see if it's present
	call	nz,SetTerminalPort	; If present, set the terminal port

NvRamValid:
; End of v5.4 mod

	LD	HL,nLine
	CALL	PutMsg			; Blank line at the start

	CALL	SMCSetup		; Initialise the SMC chip

	XOR	A			; Be sure timer is stopped
	OUT0	(TCR),A

	LD	HL,isdate
	CALL	PutMsg		; ROM Date Code
	LD	HL,sepr
	CALL	PutMsg

	CALL	ClockSpd	; Report system-clock speed
	DB	1
	CALL	TellMem		; Report available RAM

; Mod for v5.8
; Moved wait state setup here (prior to option prompt).
; Previously, wait states were only set if the option prompt
; timed out.
	LD	A,(nvRam0)	;- Get RAM0 values
	RLA			;-  Move Extra Mem & IO Wait bits
	AND	11110000B	;-   mask
	OUT0	(DCNTL),A	;-    (set)
; End of v5.8 mod

; Remove the immediate entry into boot code in favor of a short delay
; and option prompt.
;	LD	A,(NVRate)	;-
;	RLA			;- Is NV-Ram Valid?
;	JR	C,BootAll	;-   Bypass next if Not, all is Set

	LD	HL,wtSetM	;-
	CALL	PutMsg		;- Else Give chance to Setup

; Mod for v5.8
; Updated to reflect new 25Hz timer freq
; Cleaned up option selection loop
	LD	B,40		;-  Set Outer Loop for ~6 Sec
Wt000:	IN0	E,(RLDR1L)	;-
	IN0	D,(RLDR1H)	;- Get Timer Count for Inner Loop
Wt001:	CALL	GetChr		;- Any Key Pressed?
	JR	Z,Wt002		;- ..jump if Not
	LD	HL,WtCfg	;- Set HL for JP to setup
	CP	esc		;- ESCape Key?
	JR	Z,Wt003		;- ... done if so
	LD	HL,Debug	;- Set HL for JP to debug
	CP	cr		;- RETurn Key?
	JR	Z,Wt003		;- ... done if so
Wt002:	DEC	DE		;- Count down
	LD	A,D		;-
	OR	E		;-  Done?
	JR	NZ,Wt001	;- ..continue sampling if Not (inner)
	DJNZ	Wt000		;-  ..loop Outer if Not done
	LD	HL,BootAll	;- Timeout, set HL for JP and fall thru
Wt003:	PUSH	HL		;- Save JP target
	LD	A,cr		;- Back up to beginning of line
	CALL	PutChr		;-
	LD	HL,wtSetC	;-  Clear the Prompt
	CALL	PutMsg		;-
	POP	HL		;- Restore JP target
	JP	(HL)		;- ... and go
WtCfg:	CALL	TCfg		;- Enter Setup (only returns if CR entered)
; End of v5.8 mod

; Boot All devices after validating which are available

;--------------------------------------------------------------------
; ROMv5.0 Mod                                                    TAG
; Allow OS 'Load and Return' modification to load the OS and
; return to Debugger.
BootAll:
	xor	a
	ld	(OsEx),a
; End of mods
BAM:	LD	A,10			;; Ten tries to boot before going to Debug
BtLoop:	LD	(AF_),A			;;  (store in unused area)
	LD	BC,0			;;
BtDly:	DJNZ	BtDly			;; Delay
	DEC	C			;;  a
	JR	NZ,BtDly		;;   little

	CALL	InGIDE			;- Init GIDE Geometry if Drive present
	CALL	CkSCSI			;; Reset SCSI Controller, check if Present

	ld	a,(AF_)			; If not 1st time through boot loop
	cp	10
	jr	nz,EntFlopBt		;  then skip the 1st time checks

; First time though boot chain loop - check to find start point
	ld	a,(ramAry+10)		; Get NvRam byte 10 for AutoBoot start
	and	AutoBGide		; Mask all but AutoBoot bits
	jp	z,Debug			;  and enter debugger of no AutoBoot

	cp	AutoBScsi		; SCSI AutoBoot?
	jr	z,EntScsiBt
	cp	AutoBGide		; GIDE AutoBoot?
	jr	z,EntGideBt

; Must be entry into Floppy AutoBoot routine
EntFlopBt:
	CALL	ChkBrk			;- Sample Keyboard, break to Debug if KeyPress
	CALL	FdBoot			;; Attempt a Floppy Boot

; Entry into SCSI Boot Routine
EntScsiBt:
	CALL	ChkBrk			;- Sample Keyboard, break to Debug if KeyPress
	LD	A,(SCSIOk)		;;
	OR	A			;; Do we have a SCSI Controller?
	CALL	Z,BtSCSI		;;   Try Boot if Yes

; Entry into GIDE boot routine
EntGideBt:
	CALL	ChkBrk			;- Sample Keyboard, break to Debug if KeyPress
	LD	A,(PGMok)		;-
	OR	A			;- Do we have a GIDE Master?
	CALL	Z,BtGIDE		;-   Try Boot if Yes

	LD	A,(AF_)			;;
	DEC	A			;; More tries remaining?
	JR	NZ,BtLoop		;; ..loop if So
			;;..else fall thru..
	JP	Debug			; To the internal debugger

nLine:	DB	cr,0
sepr:	DB	"  ",0
wtSetM:	DB	cr,"    (ESCape to enter Setup, RETurn for Debug)",0
wtSetC:	DB	"                                               ", cr,0

;.....
; Break to Debugger if any Key Pressed

ChkBrk:	CALL	GetChr		;- Sample Keyboard
	JP	NZ,Debug	;- .Break to Debug if Key Pressed
	RET			;-   Else back to Caller

;======================================================================
; Routine to output a block of initialisation data. Table is Port, then
; Data.  A port-value of 0 terminates.
; Enter: HL -> [Port, Data] entries
; Exit : HL -> End-of-Table
 
IoInz:	PUSH	AF			; Save regs.
	PUSH	BC
	XOR	A			; Get a Zero
	LD	B,A			;  to Hi-order Address
	CPL				; 0 -> FF
IIz1:	LD	C,(HL)			; Get Port #
	TST	C			; End?
	JR	Z,IIz2			; ..exit if So
	INC	HL			; Else advance to data byte
	OUTI				;  send it
	INC	B			;   (correct B for above)
	JR	IIz1			;  ..keep going

IIz2:	POP	BC			; Get them back
	POP	AF
	RET

;======================================================================
;	P R I M A R Y    I / O    P O R T    R O U T I N E S
;======================================================================
; Output A on ESCC port

PutChr:	PUSH	AF			; Save the character
; Mods for v5.2
	ld	a,(SysParm)
	and	AltTermSel		; Test for alternate terminal
	jr	nz,AltTermOut		;  and go if alternate selected
; End of 5.2 mods
OutAx:	IN0	A,(SCCACNT)
	BIT	2,A			; Port ready? Test THRE bit
	JR	Z,OutAx			; .Loop till it is

; See if hardware flow control enabled in NvRam
	ld	a,(SysParm)	; Get Hardware Flow Control bit
	and	HfcEn
	jr	z,NoHFC			; Go if HFC disabled

; For hardware handshaking, ensure CTS (bit) is set.		TAG
	IN0	A,(SCCACNT)
	BIT	5,A			; CTS set?
	JR	Z,OutAx
; and ensure DSR input is asserted
	IN	A,(DRA)			; Read Parallel Port A data reg
	BIT	PaDSR,A
	JR	NZ,OutAx		; Go if DSR input not asserted
NoHFC:
	POP	AF
	OUT0	(SCCAD),A		; Send char.
	RET

; Mods for v5.2
; AltTermOut is a v5.2 update that allows all terminal output to be passed to the
; LAN-connected serial port instead of the local serial port on the ESCC.  The LAN-
; connected port can run as high as 230,400 baud and is far faster than the regular
; serial port.
AltTermOut:
; Check for terminal connected
	in0	a,(QuadStat)		; Test for connection
	and	AltTermConn		; See if LAN connected
	jr	z,AltTermOut		;  and loop if not connected

; Test for THRE bit
AltThreLp:
	in0	a,(AltTerm+LinSROff)	; Read Line Status Reg
	and	TransEmpty		; Test THR status
	jr	z,AltThreLp		;  and loop until THR is empty

; Wait for CTS active
AltCts:
	in0	a,(AltTerm+ModSROff)	; Read Modem Status reg
	and	CtsActive		; Test for CTS active
	jr	z,AltCts		;  and loop until CTS active

; Mods for v5.6
; Short delay between LAN output characters
	push	bc
	ld	b,10
ATO5:	djnz	ATO5
	pop	bc
; End of v5.6 mods

; Send character
	pop	af			; Restore the character
	out0	(AltTerm+ThrOff),a	; Send to the Alternate Terminal
	ret
; End v5.2 mods

;.....
; Get char. from ESCC to A (Z if none)

GetChr:
; Mods for v5.2
	ld	a,(SysParm)		; Get terminal re-director bit
	bit	1,a
	jr	nz,AltTermChrIn		;  and go if alternate selected

SerTermChrIn:
; End v5.2 mods
	IN0	A,(SCCACNT)
	AND	01H			; Take only RX-ready
	RET	Z			; .If none, exit with Z (& A=0)
	IN0	A,(SCCAD)		; Else, pull the char.
	or	a			; Set Z if null char
	RET

; Mods for v5.2
; AltTermChrIn allows keyboard input to come from the LAN-connected serial port
; rather than the local serial port connected to the ESCC.  At 115,200 baud,
; keyboard input (and display output) is far faster than the ESCC-based serial
; terminal.
AltTermChrIn:
; If no character ready, exit with A=0 and Z set
	in	a,(AltTerm+LinSROff)	; Get Line Status
	and	CharReady		; Mask with char ready bit
	ret	z			;  and exit Z if no char

; Char ready - Put char in A and exit
	in	a,(AltTerm+RbrOff)	; Read the character
	or	a			; If NULL, exit Z set to flag
	ret				;  no character
; End v5.2 mods

;======================================================================
;	  S M C    C H I P    S E T U P    R O U T I N E
;======================================================================
; NB This code is written for the FDC37C665 series parts.
; Although the hardware can run other parts, this code will
;  need altering.

; Enter Configuration Mode Command Sequence
 
EnterCfg:
	DB	CFCNTL, 55H	; Special sequence to enter mode
	DB	CFCNTL, 55H	;NB different for the '666 series
	DB	0

; Exit Configuration Mode Command Sequence

ExitCfg:
	DB	CFCNTL, 0AAH
	DB	0

;4b Macros manually expanded for correct assembly by Al Hawley's ZMAC.

cfg665:			; Config. for FDC37C665GT, -IR, and FDC37C666xx
cfg666:
;4b	Config	CR0, 38H	; IDE disabled, FDC enabled
	DB	CFCNTL, CR0	;4b IDE disabled, FDC enabled
	DB	CFDATA, 38H	;4b
;4b	Config	CR1, 85H	; Basic bi-di printer port
	DB	CFCNTL, CR1	;4b Basic bi-di printer port
	DB	CFDATA, 85H	;4b
;4b	Config	CR2, 1CH	; UART 1 at primary addr., UART 2 off
	DB	CFCNTL, CR2	;4b UART 1 at primary addr., UART 2 off
	DB	CFDATA, 1CH	;4b
;4b	Config	CR3, 78H	; Diskette: normal XT mode
	DB	CFCNTL, CR3	;4b Diskette: normal XT mode
	DB	CFDATA, 78H	;4b
;4b	Config	CR4, 00H	; No ECP/EPP parallel port modes
	DB	CFCNTL, CR4	;4b No ECP/EPP parallel port modes
	DB	CFDATA, 00H	;4b
;4b	Config	CR5, 24H	; Swap drive 0,1 lines
	DB	CFCNTL, CR5	;4b Swap drive 0,1 lines
	DB	CFDATA, 24H	;4b
	DB	0

;.....
; Configure the SMC Multi-IO Chip

SMCSetup:
	LD	HL,EnterCfg
	CALL	IoInz		; Put it into Config. mode
	LD	A,CRF
	CALL	GetOne		; CRF must = 0
	AND	A
	JR	NZ,BadChip
	LD	A,CRD
	CALL	GetOne
	LD	IX,cfg665
	CP	65H		; Must be '665 or '666
	JR	Z,Check1
	LD	IX,cfg666
	CP	66H
	JR	NZ,BadChip
Check1:	LD	B,A		; Saved chip ID
	LD	A,CRE
	CALL	GetOne
	LD	C,A		; Save subtype
	RES	7,A
	CP	2		; Must be correct
	JR	NZ,BadChip
			; Found a good chip: say so
	PUSH	BC
	LD	HL,chipM1
	CALL	PutMsg
	POP	BC
	LD	HL,chipM5
	LD	A,B
	CP	66H
	JR	NZ,Cf2
	LD	HL,chipM6
Cf2:	PUSH	BC
	CALL	PutMsg
	POP	BC
	LD	HL,chipM2
	BIT	7,C
	JR	Z,Cf3
	LD	HL,chipM3
Cf3:	CALL	PutMsg
	LD	HL,chipM4
	CALL	PutMsg
	PUSH	IX
 	POP	HL
	CALL	IoInz		; Do the configuration
	JR	CfDone

BadChip: LD	HL,chipM9
	CALL	PutMsg		; Unrecognised chip

CfDone:	LD	HL,ExitCfg	; Exit config. mode
	CALL	IoInz
	RET			; Done

chipM1:	DB	cr, "SMC IO chip identified (FDC37C66",0
chipM2:	DB	"GT)",0
chipM3:	DB	"IR)",0
chipM4:	DB	": configuring", cr,0
chipM5:	DB	"5",0
chipM6:	DB	"6",0
chipM9:	DB	cr, "Unrecognised IO chip: not configured", cr,0

; Read Configuration Register given in A

GetOne:	OUT0	(CFCNTL),A	; Point to the reg.
	IN0	A,(CFDATA)	; Get value
	RET

;======================================================================
; Output A in hexadecimal

HexA:	PUSH	DE
	PUSH	HL		; Save regs.
	LD	HL,0
	PUSH	HL
	PUSH	HL		; Local space: 4 bytes
	ADD	HL,SP		; SP -> HL
	LD	E,L
	LD	D,H		; Copy in DE
	CALL	PutA		; A-> hex. on stack
	DEC	DE
	XOR	A
	LD	(DE),A		; Terminator
	CALL	PutMsg		; Display it (from HL)
	POP	HL
	POP	HL		; Discard temp. space
	POP	HL		; Restore saved regs.
	POP	DE
	RET

; Mods for v5.2
;======================================================================
; The Port4Ini routine will init the 16C754 Quad UART ports IF a test
; of the Scratch register shows the UART is present.
;
;  If (SysParm) bit 1 = 0, then the System Terminal is on the normal
; serial port internal to the Z80182.
;
; (SysParm) is set to 00H on reset/power-on.  If the nvRam data is
; valid, then nvRam10 is copied into (SysParm) and bit 1 determines the
; System Terminal, where NZ is the LAN-based terminal.
;======================================================================
Port4Ini:
	call	TestUart		; Test for UART present
	ld	hl,SysParm
	jr	z,InitUarts		; Go if Quad UART is present

; The UART is not present, so ensure the terminal is on Primary
	res	1,(hl)			; Flag terminal on Primary
	res	2,(hl)			; Flag 4-Port adapter not present
	ret

; Init the Quad UART ports on the TL16C754 UART.  The 4 ports are identical,
; each being set to the baud rate determined by (SysParm) bits 3/4, with
; auto flow control and FIFO enabled.
InitUarts:
	set	2,(hl)			; Flag 4-Port adapter present		
	call	Init754Uart

; All UART ports are initialized.  See if the terminal is connected to
; the local serial port and, if so, exit.
	ld	a,'P'			; Flag sys terminal linked to
	out0	(AltTerm+ScrOff),a	;  Primary (local serial port)

	ld	a,(SysParm)
	and	AltTermSel		; Test terminal port
	ret	z			;  and exit if Serial Terminal active

; The 4-Port UART is present and the system Terminal is linked to the
; LAN-based terminal port.  Set the terminal port scratch register to
; 'A' for alternate terminal.
SetLanActive:
	ld	a,'A'			; Flag sys terminal linked to
	out0	(AltTerm+ScrOff),a	;  LAN port (Alternate terminal)

; The terminal is connected to the LAN port, so wait until the LAN
; connection is established before proceeding.
LanWait:
	in0	a,(QuadStat)		; Get LAN port status
	and	AltTermConn		; See if LAN connected
	ret	nz			; Exit when connected
	jr	LanWait			; Else, loop until connected

TestUart:
; Check to see if the UART is present.  Write to the scratch register,
; then read it back and test.  Complement the bits, write it, read it,
; and test. Exit NZ set if UART not present, else 55h in Scratch register.
	ld	a,0aah
	ld	b,a
	out0	(AltTerm+ScrOff),a	; Write the scratch reg
	in0	a,(AltTerm+ScrOff)
	cp	b
	ret	nz			; Exit 'NZ' if no UART
	cpl				; Compliment the bits
	ld	b,a
	out0	(AltTerm+ScrOff),a
	in0	a,(AltTerm+ScrOff)
	cp	b			; Exit 'NZ' if no UART
	ret				;  or 'Z' if UART present

;======================================================================
; Subroutine	Init754Uart - Init the enhanced 16C754 UART
;
; Uses:		A, HL, BC
;======================================================================
Init754Uart:
	ld	b,Port4TblLen/2		; Load loop counter
	ld	hl,Port4_384		; Assume 38,400 baud
	ld	a,(SysParm)
	and	Port4Baud230		; Mask baud selection bits
	ld	c,a			; Save masked port baud rate
	jr	z,Init754Lp1		; Go if 'Z' set for 38,400 baud
	ld	hl,Port4_576		; Not 38,400; assume 57,600
	ld	a,c
	cp	Port4Baud576		; test for 57,600 baud
	jr	z,Init754Lp1
	ld	hl,Port4_115		; not 57,600; assume 115,200
	ld	a,c
	cp	Port4Baud115
	jr	z,Init754Lp1
	ld	hl,Port4_230		; Not 115,200, so must be 230,400
Init754Lp1:
	ld	c,(hl)			; Get the port address
	inc	hl			; Point to the data
	otim				; (0,(C)) <-- (HL)
					; HL <- HL + 1	(INC HL)
					; C <- C + 1	(INC C)
					; B <- B - 1	(DEC B)
	jr	nz,Init754Lp1		; Z/NZ status based on DEC B by OTIM

; Use 115,200 baud divisor for longer delay
	ld	hl,HSB_1152*16*5	; Counter 16 x baud divisor
Del754:					;  with *5 added for CPU clock
	dec	hl			;  5* higher than UART clock
	ld	a,h
	or	L
	jr	nz,Del754
	ret

; 16C754 UART Init Tables.  There are four tables, one for each of the
; supported baud rates of 57,600, 76,800, 115,200, and 230,400 baud.
;
; Within each table, a single entry consists of a port address
; followed by the data to be output to that port.

; This table is used @ 38,400 baud
Port4_384:
; Port A table
	db	UartPortA + IntEROff,	0		; Disable interrupts
	db	UartPortA + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortA + DllOff,	low HSB_384	; Low byte baud divisor
	db	UartPortA + DlmOff,	high HSB_384	; High byte baud divisor
;	db	UartPortA + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortA + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortA + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortA + ScrOff,	'A'		; PortA @ 'A'

; Port B table
	db	UartPortB + IntEROff,	0		; Disable interrupts
	db	UartPortB + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortB + DllOff,	low HSB_384	; Low byte baud divisor
	db	UartPortB + DlmOff,	high HSB_384	; High byte baud divisor
;	db	UartPortB + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortB + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortB + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortB + ScrOff,	'B'		; PortB @ 'B'

; Port C table
	db	UartPortC + IntEROff,	0		; Disable interrupts
	db	UartPortC + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortC + DllOff,	low HSB_384	; Low byte baud divisor
	db	UartPortC + DlmOff,	high HSB_384	; High byte baud divisor
;	db	UartPortC + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortC + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortC + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortC + ScrOff,	'C'		; PortC @ 'C'

; Port D table
	db	UartPortD + IntEROff,	0		; Disable interrupts
	db	UartPortD + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortD + DllOff,	low HSB_384	; Low byte baud divisor
	db	UartPortD + DlmOff,	high HSB_384	; High byte baud divisor
;	db	UartPortD + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortD + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortD + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortD + ScrOff,	'D'		; PortD @ 'D'

Port4TblLen	equ	$-Port4_384

; This table is used @ 57,600 baud
Port4_576:
; Port A table
	db	UartPortA + IntEROff,	0		; Disable interrupts
	db	UartPortA + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortA + DllOff,	low HSB_576	; Low byte baud divisor
	db	UartPortA + DlmOff,	high HSB_576	; High byte baud divisor
;	db	UartPortA + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortA + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortA + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortA + ScrOff,	'A'		; PortA @ 'A'

; Port B table
	db	UartPortB + IntEROff,	0		; Disable interrupts
	db	UartPortB + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortB + DllOff,	low HSB_576	; Low byte baud divisor
	db	UartPortB + DlmOff,	high HSB_576	; High byte baud divisor
;	db	UartPortB + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortB + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortB + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortB + ScrOff,	'B'		; PortB @ 'B'

; Port C table
	db	UartPortC + IntEROff,	0		; Disable interrupts
	db	UartPortC + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortC + DllOff,	low HSB_576	; Low byte baud divisor
	db	UartPortC + DlmOff,	high HSB_576	; High byte baud divisor
;	db	UartPortC + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortC + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortC + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortC + ScrOff,	'C'		; PortC @ 'C'

; Port D table
	db	UartPortD + IntEROff,	0		; Disable interrupts
	db	UartPortD + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortD + DllOff,	low HSB_576	; Low byte baud divisor
	db	UartPortD + DlmOff,	high HSB_576	; High byte baud divisor
;	db	UartPortD + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortD + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortD + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortD + ScrOff,	'D'		; PortD @ 'D'


; This table is used @ 115,200 baud
Port4_115:
; Port A table
	db	UartPortA + IntEROff,	0		; Disable interrupts
	db	UartPortA + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortA + DllOff,	low HSB_1152	; Low byte baud divisor
	db	UartPortA + DlmOff,	high HSB_1152	; High byte baud divisor
;	db	UartPortA + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortA + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortA + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortA + ScrOff,	'A'		; PortA @ 'A'

; Port B table
	db	UartPortB + IntEROff,	0		; Disable interrupts
	db	UartPortB + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortB + DllOff,	low HSB_1152	; Low byte baud divisor
	db	UartPortB + DlmOff,	high HSB_1152	; High byte baud divisor
;	db	UartPortB + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortB + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortB + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortB + ScrOff,	'B'		; PortB @ 'B'

; Port C table
	db	UartPortC + IntEROff,	0		; Disable interrupts
	db	UartPortC + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortC + DllOff,	low HSB_1152	; Low byte baud divisor
	db	UartPortC + DlmOff,	high HSB_1152	; High byte baud divisor
;	db	UartPortC + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortC + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortC + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortC + ScrOff,	'C'		; PortC @ 'C'

; Port D table
	db	UartPortD + IntEROff,	0		; Disable interrupts
	db	UartPortD + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortD + DllOff,	low HSB_1152	; Low byte baud divisor
	db	UartPortD + DlmOff,	high HSB_1152	; High byte baud divisor
;	db	UartPortD + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortD + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortD + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortD + ScrOff,	'D'		; PortD @ 'D'
; End of v5.2 mods

; v5.3 mods
; This table is used @ 230,400 baud
Port4_230:
; Port A table
	db	UartPortA + IntEROff,	0		; Disable interrupts
	db	UartPortA + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortA + DllOff,	low HSB_2304	; Low byte baud divisor
	db	UartPortA + DlmOff,	high HSB_2304	; High byte baud divisor
;	db	UartPortA + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortA + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortA + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortA + ScrOff,	'A'		; PortA @ 'A'


; Port B table
	db	UartPortB + IntEROff,	0		; Disable interrupts
	db	UartPortB + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortB + DllOff,	low HSB_2304	; Low byte baud divisor
	db	UartPortB + DlmOff,	high HSB_2304	; High byte baud divisor
;	db	UartPortB + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortB + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortB + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortB + ScrOff,	'B'		; PortB @ 'B'


; Port C table
	db	UartPortC + IntEROff,	0		; Disable interrupts
	db	UartPortC + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortC + DllOff,	low HSB_2304	; Low byte baud divisor
	db	UartPortC + DlmOff,	high HSB_2304	; High byte baud divisor
;	db	UartPortC + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortC + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortC + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortC + ScrOff,	'C'		; PortC @ 'C'


; Port D table
	db	UartPortD + IntEROff,	0		; Disable interrupts
	db	UartPortD + LinCROff,	10000011b	; Enable Divisor Latch
	db	UartPortD + DllOff,	low HSB_2304	; Low byte baud divisor
	db	UartPortD + DlmOff,	high HSB_2304	; High byte baud divisor
;	db	UartPortD + FifoCROff,	10000111b	; Rcv FIFO trigger to 56 chars
							;  Xmit FIFO trigger to 8 chars
							;  Reset Rcvr & Trans FIFOs
							;  Enable FIFO
	db	UartPortD + LinCROff,	00000011b	; 8N1 Operating param, DLAB off
	db	UartPortD + ModCROff,	00000011b	; DTR + RTS enabled
	db	UartPortD + ScrOff,	'D'		; PortD @ 'D'

; End of v5.3 mods

; v5.4 mods
;======================================================================
;  Invalid NvRam; the 4-Port adapter is detected and initialized.
; Determine which port (Serial or LAN) will be used for the terminal.
; This code is only entered when the NvRam is invalid on startup AND
; the 4-Port LAN adapter is present.
;
; Here's the algorithm we're using to determine the correct terminal
; port:
;
;	Send a "Press Return..." msg via the Serial Terminal
;
; SerialCharLoop:
;	Check for a response
;	  If CR or LF is received, do nothing and exit
;	Check for the LAN connected
;	  Loop back to SerialCharLoop if LAN not connected
;
; LanConnected:
;	Send "Press Return..." msg via the LAN terminal
;
; CharLoop:
;	Check for Serial Terminal response
;	  If CF or LF recevied, do nothing and exit 
;	Check for LAN Terminal response
;	  If CR or LF received, set LAN Term active and exit
;	Loop back to CharLoop	
;
;======================================================================
SetTerminalPort:
	ld	hl,PrsRetMsg		; Point to "Press Return..." msg
SerMsgLp:
	ld	a,(hl)
	inc	hl
	or	a
	jr	z,SerMsgDone
	ld	de,SerMsgLp		; Push the return address
	push	de
	push	af			;  and the character to display
	jp	OutAx			; Send char via Serial terminal

; Msg sent via Serial Terminal
SerMsgDone:
	call	SerTermChrIn		; Get serial character
	jr	z,NoSerChar		;  and loop if no char

; Got character - is it a CR?
	cp	cr
	ret	z			; Do nothing and exit if CR received
	cp	lf
	ret	z			; Do nothing and exit if LF received
; Dump whatever was received - garbage - and fall though

; Nothing from Serial Terminal - check for LAN connected
NoSerChar:
	in0	a,(QuadStat)		; Test for connection
	and	AltTermConn		; Test for LAN connected
	jr	z,SerMsgDone		;  and go if not connected
	
; The LAN is connected.  Send the message via the LAN
LanConnected:
	ld	hl,PrsRetMsg		; Point to "Press Return..." msg
LanMsgLp:
	ld	a,(hl)
	inc	hl
	or	a
	jr	z,LanMsgDone
	ld	de,LanMsgLp		; Push the return address
	push	de
	push	af			;  and the character to display
	jp	AltTermOut		; Send char via LAN terminal

LanMsgDone:
	call	SerTermChrIn
	jr	z,GetLanChar
	cp	cr
	ret	z			; Do nothing and exit if CR 
	cp	lf
	ret	z			; Do nothing and exit if LF 
; Dump whatever was received - garbage - and fall through

GetLanChar:
	call	AltTermChrIn		; Get LAN character
	jr	z,LanMsgDone		;  loop if no character

	cp	cr
	jr	z,LanCharGood		; Go if CR received
	cp	lf
	jr	nz,LanMsgDone		; Go if not LF

; Received a LF or CR via the LAN terminal
LanCharGood:
	ld	a,(SysParm)		; Get control parameter
	or	AltTermSel		; Flag LAN terminal selected
	ld	(SysParm),a
	jp	SetLanActive		; Tell BIOS to use LAN terminal

PrsRetMsg:	db	cr,lf,"Press Return... ",0
; End of v5.4 mods

;======================================================================
; Measure the system-clock speed.
; Measurement is done by setting the 2ary serial port to 1200bps, and
; outputting a 10-bit character (actually NUL).  This interval equates
; to a frequency of 1200/10 = 120Hz.  This period is measured using the
; Z180 system timer, which counts CLK/20. The final value will then be
; SYSCLK / (20 * 120). This will be a 65k complement, as the timer
; counts down. The measurement will be subject to some error, but near
; enough to identify the standard crystals:
;	12.288MHz, 16.0MHz, 18.432MHz, and 24.576MHz.
;
; The estimated speed is reported on the monitor, and the Timer-1 reload
; register is set up to yield a "tick" rate of 25Hz.  The "tick" is NOT
; actually initiated.

ClockSpd:
	IN0	A,(_LSR)	; First, be sure it's empty
	BIT	6,A
	JR	Z,ClockSpd
	LD	HL,tmInz	; Ready to run it
	CALL	IoInz		; Kick off SIO & timer

ClkW:	IN0	A,(_LSR)	; Now wait till it's gone
	BIT	6,A
	JR	Z,ClkW

	IN0	L,(TMDR1L)	; Get residual timer into HL
	IN0	H,(TMDR1H)

	XOR	A		; Then kill the timer (at startup,
	OUT0	(TCR),A		;  we know it's not shared)
	IN0	A,(_RBR)	; Clear the looped char.

	INC	H		; Round HL in top 7 bits
	RES	0,H		; Further rounding
	LD	B,tCnt		; No. of table items
	LD	A,H
	LD	HL,trTab
	LD	DE,lTab
Tr1:	CP	(HL)		; Matched?
	JR	Z,Tr2
	ADD	HL,DE		; No: next
	DJNZ	Tr1
	LD	HL,oddOne-1	; Offset for INC below
Tr2:	INC	HL		; Point at the timer value
	LD	A,(HL)
	OUT0	(RLDR1L),A
	INC	HL
	LD	A,(HL)
	OUT0	(RLDR1H),A	; Set-up the timer

	INC	HL
	LD	A,(NVRate)	;; Get Rate/Valid Flag
	BIT	7,A		;; Valid?
	JR	Z,RateOk	;; ..jump if So
	LD	A,5		;; Else default to 9600 bps
RateOk:	PUSH	DE		;; Save regs
	PUSH	HL		;;
	ADD	A,A		;; Double index value
	LD	E,A		;;
	LD	D,0		;;
	ADD	HL,DE		;;  Pt to Divisor Word
	LD	E,(HL)		;;   fetch
	INC	HL		;;
	LD	D,(HL)		;;
	LD	A,WR12
	OUT0	(SCCACNT),A
	OUT0	(SCCACNT),E
	LD	A,WR13
	OUT0	(SCCACNT),A
	OUT0	(SCCACNT),D	; Set a correct baud-rate
 
	POP	HL		;; Restore Ptr to Divisor Table Start
	LD	DE,16		;;
	ADD	HL,DE		;;  Advance past table to String Addrs
	POP	DE		;;
	IN0	A,(SCR)		; Is the ROM Phantomed?
	BIT	3,A
	LD	A,(HL)		; Value for Phantom
	INC	HL
	JR	NZ,Nph
	LD	A,(HL)		; Value if Not Phantomed
Nph:	OUT0	(DCNTL),A	; Define Memory & IO Wait States
	INC	HL		;
	LD	A,(HL)
	INC	HL
	LD	H,(HL)
	LD	L,A		; Msg. pointer in hl
	OR	H		; Zero if none
	RET	Z
	POP	DE
	LD	A,(DE)		; In-line flag
	INC	DE
	PUSH	DE		; Replace link
	AND	A		; If zero, no messages
	RET	Z
	PUSH	HL		; Else, output reports
	LD	HL,xtMs1
	CALL	PutMsg
	POP	HL
	CALL	PutMsg		; The observed frequency
	LD	HL,xtMs2
	CALL	PutMsg
	RET			; Then done

;
; Timer: Timer_Int_Freq = CPU_Freq / (20 * Timer_Reload_Counter)
;;;;;; For 25Hz, Timer_Reload_Counter = CPU_Freq / 20 / 25
; For 50Hz, Timer_Reload_Counter = CPU_Freq / 20 / 50
;
; The normal timer interrupt frequency is 25Hz.  However, at 36.864MHz
; the 16 bit reload counter is too small to set a value that results in
; 25Hz, so for 36.864MHz, the timer interrupt is set to 50Hz operation.
;
; Divisors for Baud Rate / CPU Frequency combinations:
;
; TC = (PHI / (2 * Baud * 16)) - 2
;
; CPU     --------------------- Baud Rate -------------------------------
; Freq      38400   19200    9600    4800    2400    1200     600     300
; ------- ------- ------- ------- ------- ------- ------- ------- -------
;  6.144     3.00    8.00   18.00   38.00   78.00  158.00  318.00  638.00
;  8.000     4.51   11.02   24.04   50.08  102.17  206.33  414.67  831.33 (16.0000 / 2)
;  9.216     5.50   13.00   28.00   58.00  118.00  238.00  478.00  958.00 (18.4320 / 2)
; 11.0592    7.00   16.00   34.00   70.00  142.00  286.00  574.00 1150.00 (22.1184 / 2)
; 12.288     8.00   18.00   38.00   78.00  158.00  318.00  638.00 1278.00 (24.5760 / 2)
; 14.7456   10.00   22.00   46.00   94.00  190.00  382.00  766.00 1534.00 (29.4912 / 2)
; 16.000    11.02   24.04   50.08  102.17  206.33  414.67  831.33 1664.67 (16.0000 * 1)
; 18.432    13.00   28.00   58.00  118.00  238.00  478.00  958.00 1918.00 (18.4320 * 1)
; 22.1184   16.00   34.00   70.00  142.00  286.00  574.00 1150.00 2302.00 (22.1184 * 1)
; 24.576    18.00   38.00   78.00  158.00  318.00  638.00 1278.00 2558.00 (24.5760 * 1)
; 29.4912   22.00   46.00   94.00  190.00  382.00  766.00 1534.00 3070.00 (29.4912 * 1)
; 32.000    24.04   50.08  102.17  206.33  414.67  831.33 1664.67 3331.33 (16.0000 * 2)
; 36.864    28.00   58.00  118.00  238.00  478.00  958.00 1918.00 3838.00 (18.4320 * 2)
; 44.2368   34.00   70.00  142.00  286.00  574.00 1150.00 2302.00 4606.00 (22.1184 * 2)
; 49.152    38.00   78.00  158.00  318.00  638.00 1278.00 2558.00 5118.00 (24.5760 * 2)
; 58.9824   46.00   94.00  190.00  382.00  766.00 1534.00 3070.00 6142.00 (29.4912 * 2)

; v5.8 adds new clock frequencies that can be detected and switches the
; timer frequency from 25Hz to 50Hz to avoid overflowing the reload counters
; at higher clock frequencies.

xtMs1:	DB	"CPU clock: ",0
xtMs2:	DB	"MHz", cr,0

; Translation table for results
trTab:	DB	0F6H		; 6.144MHz					(-0AH)
	DW	6144		; 50Hz Timer
	DW	638, 318, 158, 78	; Divisors for 300,600,1200,2400 bps
	DW	 38,  18,   8,  3	;	4800,9600,19200,38400 bps
	DB	10H		; Wait States: ROM Phantomed
	DB	10H		;   Ditto: Physical ROM
	DW	ms6		; Ptr. to message
lTab	 EQU  $-trTab

	DB	0F4H		; 8.000MHz					(-0CH)
	DW	8000		; 50Hz Timer
	DW	831, 415, 206, 102
	DW	 50,  24,  11,   4	; NOTE: Inaccurate @ 38.4 kbps
	DB	10H
	DB	10H
	DW	ms8

	DB	0F2H		; 9.216MHz					(-0EH)
	DW	9216		; 50Hz Timer
	DW	958, 478, 238, 118
	DW	 58,  28,  13,   5	; NOTE Inaccurate @ 38.4 kbps
	DB	20H
	DB	20H
	DW	ms9

	DB	0EEH		; 11.0592MHz					(-12H)
	DW	11059		; 50Hz Timer
	DW	1150, 574, 286, 142
	DW	  70,  34,  16,   7
	DB	20H
	DB	20H
	DW	ms11

	DB	0ECH		; 12.288MHz					(-14H)
	DW	12288		; 50Hz Timer
	DW	1278, 638, 318, 158
	DW	  78,  38,  18,   8
	DB	20H
	DB	20H
	DW	ms12

	DB	0E8H		; 14.7456MHz					(-18H)
	DW	14746		; 50Hz Timer
	DW	1534, 766, 382, 190
	DW	  94,  46,  22,  10
	DB	20H
	DB	20H
	DW	ms15

	DB	0E6H		; 16.000MHz					(-1AH)
	DW	16000		; 50Hz Timer
	DW	1665, 831, 415, 206
	DW	 102,  50,  24,  11
	DB	20H
	DB	20H
	DW	ms16

	DB	0E2H		; 18.432MHz					(-1EH)
	DW	18432		; 50Hz Timer
	DW	1918, 958, 478, 238
	DW	 118,  58,  28,  13
	DB	60H
	DB	0A0H
	DW	ms18

	DB	0DCH		; 22.1184MHz					(-24H)
	DW	22118		; 50Hz Timer
	DW	2302, 1150, 574, 286
	DW	 142,   70,  34,  16
	DB	70H
	DB	0F0H
	DW	ms22

	DB	0D8H		; 24.576MHz					(-28H)
	DW	24576		; 50Hz Timer
	DW	2558, 1278, 638, 318
	DW	 158,   78,  38,  18
	DB	70H
	DB	0F0H
	DW	ms24

	DB	0D0H		; 29.4912MHz					(-30H)
	DW	29491		; 50Hz Timer
	DW	3070, 1534, 766, 382
	DW	 190,   94,  46,  22
	DB	70H
	DB	0F0H
	DW	ms29

	DB	0CCH		; 32.000MHz					(-34H)
	DW	32000		; 50Hz Timer
	DW	3331, 1665, 831, 415
	DW	 206,  102,  50,  24
	DB	70H
	DB	0F0H
	DW	ms32

	DB	0C4H		; 36.864MHz					(-3CH)
	DW	36864		; 50Hz timer
	DW	3838, 1918, 958, 478
	DW	 238,  118,  58,  28
	DB	70H
	DB	0F0H
	DW	ms36

	DB	0B8H		; 44.2368MHz					(-48H)
	DW	44237		; 50Hz timer
	DW	4606, 2302, 1150, 574
	DW	 286,  142,   70,  34
	DB	70H
	DB	0F0H
	DW	ms44

	DB	0B0H		; 49.152MHz					(-50H)
	DW	49152		; 50Hz timer
	DW	5118, 2558, 1278, 638
	DW	 318,  158,   78,  38
	DB	70H
	DB	0F0H
	DW	ms48

	DB	0A0H		; 58.9824MHz					(-60H)
	DW	58982		; 50Hz timer
	DW	6142, 3070, 1534, 766
	DW	 382,  190,   94,  46
	DB	70H
	DB	0F0H
	DW	ms58
tCnt	 EQU  ($-trTab)/lTab	; No. of entries

oddOne:	DW	34000		; Default timer for odd crystals
	DW	1665, 831, 415, 206
	DW	 102,  50,  24,  11
	DB	70H		; Assume worst-case
	DB	0F0H		;   Max Wait States
	DW	unkmsg		; Unknown Rate Message
	
ms6:	DB	"6.144",0
ms8:	DB	"8.000",0
ms9:	DB	"9.216",0
ms11:	DB	"11.0592",0
ms12:	DB	"12.288",0
ms15:	DB	"14.7456",0
ms16:	DB	"16.000",0
ms18:	DB	"18.432",0
ms22:	DB	"22.1184",0
ms24:	DB	"24.576",0
ms29:	DB	"29.4912",0
ms32:	DB	"32.000",0
ms36:	DB	"36.864",0
ms44:	DB	"44.2368",0
ms48:	DB	"49.152",0
ms58:	DB	"58.9824",0

unkmsg:	DB	"??-Unknown-??",0	;; Use for Non-Standard/Un-determined

tmInz:	DB	_LCR, 80H	; DLAB=1
	DB	_DDL, 60H	; 96D, divisor for 1200bps
	DB	_DLM,  0H
	DB	_LCR, 03H	; DLAB=0, 8 bits, no parity
	DB	_MCR, 10H	; Loopback, so nothing is output
	DB	TMDR1L, 0
	DB	TMDR1H, 0	; Timer-1: preset to zero
	DB	RLDR1L, 0
	DB	RLDR1H, 0
	DB	TCR,	02H	; Kick off the timer
	DB	_THR,   0	; Send the byte
	DB	0		; Stop flag

;======================================================================
; Report amount of RAM available
 
TellMem: IN0	A,(RAMUBR)	; Upper bound
	ADD	A,1		; To 1st Invalid Page (Set CY)
	RRA			;  Scale & Keep CY
	IN0	L,(RAMLBR)	; Lower Bound
	SRL	L		;  Scale Similarly
	SUB	L		;   Available RAM / 8kB
	LD	L,A
	LD	H,8
	MLT	HL		; HL = size * 1kB
 
	PUSH	HL
	LD	HL,rams1	; Now output report
	CALL	PutMsg
	POP	HL
	CALL	DecOut
	LD	HL,rams2
	CALL	PutMsg
	IN0	A,(RAMLBR)
	CALL	HexA		; Lower bound
	LD	HL,rams3
	CALL	PutMsg
	IN0	A,(RAMUBR)
	CALL	HexA
	LD	HL,rams4
	CALL	PutMsg

	IN0	A,(SCR)
	BIT	3,A		; ROM shadowed?
	LD	HL,rams5
	CALL	NZ,PutMsg	; Say so
	LD	HL,nLine
	CALL	PutMsg
	RET

rams1:	DB	"RAM available: ",0
rams2:	DB	"kB. From ",0
rams3:	DB	"000 to ",0
rams4:	DB	"FFF",0
rams5:	DB	cr, "        ROM shadowed into 1st 32kB",0

;======================================================================
; Initialisation tables

inzTab:	DB	RCR,	00	; No refresh (all static)
	DB	OMCR,	00	; Timings as Z80, or
	DB	IL,	00	; Local interrupts vector base
;	DB	WSGCS,  9BH	;  CS waits don't work??
	DB	DCNTL,  70H	; Wait states:	mem 1 (for ROM), IO 4 (for SMC)
				;  (This is Re-Set later or if NVRAM Valid!)
	  IF  !DBUG
	DB	ROMBR,	07H	; ROM in 1st 32kB
	  ENDIF		; ~ DBUG

	 IFDEF  lanalyse
	DB	SCR,	70H	; Logic analyser support, ROM mapped
	 ELSE
	DB	SCR,	60H	; No unnecessary outputs, ROM mapped
	 ENDIF

	  IF  !DBUG
	DB	RAMLBR,  08H	; RAM starts above ROM
	DB	RAMUBR, 0FFH	; RAM to the top, for now
	DB	CBAR,   0C8H	; Memory mapping:
				; Common 0: 0000-7FFF: 00000-07FFF
				; Bank:	8000-BFFF: 08000-0BFFF
				; Common 1: C000-FFFF: 08000-0BFFF
	DB	BBR,	 00 
	DB	CBR,    0FCH	; Wrap CA1 over BA, at 08000
	  ENDIF		; ~ DBUG

	DB	INTYPE,  5CH	; INT1,2 level: MRD/WR, IOCS
;-	DB	CCR,	 80H	; Full clock speed (16MHz)

	DB	ITC,	 39H	; Block SMC interrupts, yet
	DB	TCR,	 00	; Make sure the timer is stopped
	DB	DRA,	0A0H	; Initial values: Vpp off
	DB	DDRA,	 41H	; PA6 & 0 are in, others out
	DB	0	;End mark

inzTb1:	DB	CCR,	 80H	;- Full Clock Speed (If NVRam invalid/absent)
; Mod for v5.8
; Also set default clock multiplier
	DB	CMR,	 00H	;- Disable clock multiplier
	DB	0	;End mark
; End of v5.8 mod

			; ESCC Serial Channel
iTab:	DB	itAbend-$-1	; Length of table
	DB	WR4,  01000100B ; x16, 1 stop bit, no parity
	DB	WR1,  00000100B ; Parity is special RX condition
	DB	WR3,  11000000B ; RX 8 bits/char
	DB	WR5,  01100000B ; TX 8 bits/char
	DB	WR9,  00000001B ; Status affects int. vector
	DB	WR11, 01010110B ; RX & TX <- BRG, RTxC <- BRG
	DB	WR12, 50	; Baud rate divisor LSB: 9600bps
	DB	WR13, 0		;  Ditto,  MSB
	DB	WR14, 01100010B ; BRG source (internal) DPLL off
	DB	WR14, 00000011B ; BRG enabled
	DB	WR1,  00000100B ; Enable ints. here, if reqd.
	DB	WR15, 00000000B ; No "advanced" features
	DB	WR0,  00010000B ; Reset pending external ints.
	DB	WR0,  00010000B ; Repeat, to be sure of it
	DB	WR3,  11000001B ; RX enabled
; Next two lines is a change for ROMv5.0 to enable DTR signal
;	DB	WR5,  01101010B ; TX enabled, RTS active
	DB	WR5,  11101010B	; DTR enabled, 8 bits, TX enabled, RTS active
itAbend	 EQU  $

	SUBTITLE	"Memory Chip Location"
	NEWPAGE
;======================================================================
;   M E M O R Y - C H I P   L O C A T I O N   F U N C T I O N
;======================================================================
; Determine memory Size, & make setups

; Memory chips may be fitted in several combinations. The only rule is that
; if 2 RAMs are used, both must be of the same size.  The code will attempt
; to "phantom" the ROM into low RAM. This requires:
;	1. RAM is available at physical address 00000
;	2. Not less than 256kB of RAM are fitted

; The setup follows this decision table:

;Chips fitted    Address responses       RAM valid at    RAM Bounds
;U2      U3      A19 A18 A17 A16 A15     F0000   08000   Low     High
;--------------------------------------------------------------------
;--      --       x   x   x   x   x        N       N     No RAM: Halt
;32kB    --       x   x   x   x   0        Y       N     00      07
;--      32kB     x   x   x   x   1        N       Y     08      0F
;32kB    32kB     x   x   x   x   -        Y       Y     00      0F
;128kB   --       x   x   0   -   -        N       Y     00      1F
;--      128kB    x   x   1   -   -        Y       N     20      3F
;128kB   128kB    x   x   -   -   -        Y       Y     00      3F
;512kB   --       0   -   -   -   -        N       Y     00      7F
;--      512kB    1   -   -   -   -        Y       N     80      FF
;512kB   512kB    -   -   -   -   -        Y       Y     00      FF

;ROM (32kB)       0   0   0   0   0 (selected by ROMBR decoder)

; where "x" = address-bit not decoded (addresses will wrap)
;   and "-" = a fully decoded address bit.

;  The test starts by marking all addresses as valid-RAM (the ROM will
; override in its area). Then test for RAM at 2 addresses (see above):
; F0000 and 08000. This indicates if any RAM is present at all (!), and
; whether it extends down to address 00000.
;  The detailed test is then copied into RAM (since we now know the
; address of at least 1 block), and executed there, with the ROM disabled.
; The lowest and highest valid RAM addresses are determined, and set up
; in the CPU's hardware decoder.
;  Finally, if RAM extends over at least 00000-7FFFF, the ROM is copied to
; low RAM, and "phantom-ed" there, to speed up processing (since RAM is
; faster).

; As this routine starts, there is no memory set up, so no stack.

; Map registers affected:-
;	Register       Locn    Start value     Exit value
;	CBAR           3A      C8              88
;	BBR            39      00              Point into RAM as found
;	CBR            38      00               ditto
;	RAMLBR         E7      00              Low RAM bound
;	RAMUBR         E6      FF              High RAM bound
;	SCR            EF      60 (typ.)       Bit-3 set if ROM shadowed

; Finally, the RAM will be relocated up the 1MB space, if
;	1. Physical RAM base = 00000, and
;	2. The ROM is NOT shadowed.
; This renders accessible the 32kB of RAM that was previously hidden under
; the ROM, increasing total capacity.

;  Call this routine using the XCALL  IY, macro, since no stack is
; available at the time of the call (this routine will define one).

; The final setup will (assuming 64kB or more RAM) put the 3 areas thus:
;	CA0	Logical 0000-7FFF	ROM			(32K)
;	BA	Logical 8000-BFFF	RAM: base+(8000-BFFF)	(16K)
;	CA1	Logical C000-FFFF	RAM: base+(C000-FFFF)	(16K)

;  This setup is "MP/M ready", in that it is only necessary to change CBAR
; from C8 (the value set by MEMSETUP), to C0, in order to replace the ROM
; (or its phantom equivalent) by user RAM space. To select different user
; areas, just change BBR to point to another 48kB "page", anywhere in RAM
; above the 64kB base-user area. So BBR = CBR, or >= CBR+10H.

RELOCN	 EQU   40H		; RAM relocation factor (* 4kB)

MemSetup:
	XOR	A
	OUT0	(RAMLBR),A
	LD	A,-1
	OUT0	(RAMUBR),A	; Be sure RAM is valid everywhere

	LD	DE,0
	LD	A,0F0H
;	XCall	IX,xIsMem	; See if this is RAM
	ld	ix,MsRet1
	jp	xIsMem
MsRet1:
	JR	NZ,Mx1
	LD	E,0F0H		; Yes...
Mx1:	LD	A,8
;	XCall	IX,xIsMem
	ld	ix,Mx1Ret
	jp	xIsMem
Mx1Ret:
	JR	NZ,Mx2
	LD	D,8
Mx2:	LD	A,D
	OR	E		; One must have worked
	JR	NZ,Mx3
	HALT		; No RAM in the system!

Mx3:	LD	A,D
	AND	A		; Is D valid
	JR	NZ,Mx4
	LD	A,E		; Then E is
Mx4:	SUB	8		; Adjust for bank base =8000
	OUT0	(BBR),A 	; Bank: 8000 logical -> 08000/F0000
	EX	AF,AF'  	; Save BBR for later
	LD	HL,MemRtn	; Clear of the bottom, in new space
	LD	SP,HL		; Define a stack
	EX	DE,HL
	LD	HL,xMBase
	LD	BC,MEMRSZE
	LDIR			; Copy slave routine into RAM
	LD	HL,0
	LD	BC,8000H	; Compute ROM checksum
	XOR	A
Mx5:	ADD	A,(HL)
	CPI			; HL++, BC--, set P/V
	JP	PE,Mx5

	CALL	MemRtn		; Go run it (checksum in A)

;  Now assign the RAM bounds as determined by the call.  They must not be
; set earlier, as this may (temporarily) render the RAM inaccessible (the
; ROM overlays). If this happens while we are executing from RAM: disaster!

	OUT0	(RAMLBR),D
	OUT0	(RAMUBR),E	; Physical RAM extent

	IN0	A,(RAMLBR)
	AND	A		; RAM present from zero?
	JR	NZ,Mx6	  	; .No relocn. otherwise
	IN0	A,(SCR)
	BIT	3,A		; Nor if ROM is shadowed
	JR	NZ,Mx6

	LD	B,RELOCN	; OK: relocate the RAM

	IN0	A,(RAMUBR)	; Adjust Upper RAM Limit reg
	ADD	A,B		;  by increment in B
	OUT0	(RAMUBR),A
	IN0	A,(RAMLBR)	; Adjust Lower RAM Limit reg
	ADD	A,B		;  by increment in B
	OUT0	(RAMLBR),A

;  Now, set the base of CA1 for the RAM. If 64kB or more, set up logical
; addr. 0000 -> CA1. If only 32kB, set logical 8000 -> CA1, since ROM
; cannot run otherwise. BA is set similarly.

Mx6:	LD	C,0		; Adjustment
	IN0	B,(RAMLBR)
	IN0	A,(RAMUBR)
	SUB	B		; RAM range available
	CP	8		; More than 32kB?
	JR	NC,Mx7
	LD	C,8
Mx7:	LD	A,B		; ie RAMLBR
	SUB	C		; Adjust base if only 32kB

	IN0	C,(SCR)		; Is the ROM shadowed to 1st 32kB of RAM?
	BIT	3,C
	JR	Z,Mx8
	ADD	A,8		; If so, locate user RAM above it
Mx8:	OUT0	(CBR),A		; CA1 base value
	OUT0	(BBR),A		; Bank Area ditto

	LD	A,0C8H		; 0000-7FFF = CA0: ROM
	OUT0	(CBAR),A	; 8000-BFFF = BA:  RAM
				; C000-FFFF = CA1: RAM
	XRetn	IY		; .Back (called without stack)

;======================================================================
; Set the ORG @ 0B00h so the Linker can relocate the RamCheck
; segment to a standard ROM location.  The ORG address can
; be as low as 0950H - plenty of space is left available for
; future code expansion.

	org	0B00h

xMBase	EQU   $			; Base of moving code
xIsMem	EQU   $+3		; Addr. of ISMEM, locally

;	.PHASE  8200H
;
;  ZDS 3.68 does not recognize the PHASE psuedo-op, so an alternate
; method of code relocation is required.  First, name the code segment
; that is being relocated, and set the ORG to 8200H, which is the
; execution address after relocation.  This is done near the
; beginning of the program with "DEFINE RamCheck, ORG=8200h".
;
;  At the beginning of the relocated code segment, put the segment
; in context with the "segment RamCheck" psuedo-op.
;
;  To terminate the segment being relocated, use the SEGMENT psuedo-op
; to return to the MainRom segment, followed by an ORG statement to
; leave an uninitialized block of RAM that is sufficient to hold the
; entire RamCheck segment:
;
;	segment	MainRom
;	org	$+MEMRSZE
;
;  The MEMRSZE equate is defined at the end of the RamCheck code block.
;
;  The code block will be compiled at 8200H, and will reside at
; that address in the CPU memory space.  To make the segment
; contiguous with the ROM space and to store it in the reserved EEPROM
; storage block, the Linker has to be told to copy the segment from 8200H
; to the specified location in the ROM memory space.
;
;  The ORG statement above defines the address that the Linker
; must place the segment copy.  This is manually set in ZDS 3.68
; with the Project --> Settings --> Linker tab, using the Copies
; pull-down entry.  Set the copy process to copy segment RamCheck
; into ROM address space, at the specific location set by the ORG
; statement above (0B00H for ROM v5.8.)

; Select the relocated memory segment with ORG at 8200h
	segment	RamCheck

MemRtn:	JP	MStart		; Hook the main routine

IsMem:	EXX
	LD	B,A		; Save A (without stack)
	LD	HL,0C000H
	SUB	0CH		; Adjust for CB1 base
	OUT0	(CBR),A
	LD	C,(HL)		; Save original memory contents
	LD	A,55H
	LD	(HL),A		; Write to "RAM"
	CP	(HL)		; Correct?
	JR	NZ,IsM1
	CPL
	LD	(HL),A		; Try a new pattern
	CP	(HL)
IsM1:	LD	(HL),C		; Restore memory contents
	LD	A,B		; Recover A, leave F
	EXX
	XRetn	IX

mBase:  DB	00, 08, 20H, 80H, -1	; Putative base addresses

MStart:	PUSH	AF		; Save ROM checksum
	PUSH	IY		; Save IY (incoming "link")
	IN0	A,(SCR)
	SET	3,A
	OUT0	(SCR),A		; ROM turned off
	LD	IY,mBase
Ms2:	LD	A,(IY)
	INC	IY
	CP	-1
	JR	NZ,Ms1
	HALT			; Unable to locate RAM base

; Scan memory, looking for an address "wrap-around", as
; we hit the un-decoded high address bits.

Ms1:
;	XCall	IX,IsMem
	ld	ix,Ms1Ret
	jp	IsMem
Ms1Ret:
	JR	NZ,Ms2		; Physical base
	POP	IY		; Restore IY (main "link")

	LD	D,A		; Make 2 copies: fixed base
	LD	E,A		;		 moving ptr.
	SUB	0CH		; Offset adjust
	OUT0	(CBR),A
	LD	HL,0C000H	; Base of CA1
	LD	(HL),55H	; Test pattern at base
Ms4:	LD	A,8
	ADD	A,E		; Next 32kB block
	LD	E,A
	JR	C,Ms3		; Oflo from top of 1MB
;	XCall	IX,IsMem	; Is there RAM here?
	ld	ix,Ms4Ret
	jp	IsMem
Ms4Ret:
	JR	NZ,Ms3		; Out if not, else...
	LD	(HL),0		; Try to overwrite the base
	LD	A,D
	SUB	0CH		; To fixed base
	OUT0	(CBR),A
	LD	A,(HL)
	CP	55H		; Signature still there?
	JR	Z,Ms4		; .Yes: repeat

Ms3:	DEC	E		; E points at last good block

	IN0	A,(SCR)
	RES	3,A
	OUT0	(SCR),A		; ROM active again

	POP	IX		; Balance stack in case we jump to MS6
	LD	A,D
	AND	A		; RAM from 00000 (underlays ROM)?
	JR	NZ,Ms6		; No: can't shadow ROM
	LD	A,E
	SUB	D		; Amount of RAM present
	CP	3FH
	JR	C,Ms6		; Don't shadow if < 256kB of RAM
	PUSH	IX		; Put AF back on stack: this branch

; Copy the ROM into RAM, and finally disable it

	PUSH	DE		; Save RAM bounds
	LD	BC,8000H	; Length of ROM block
	LD	HL,0		; Start address
	LD	D,0		; Sumcheck for later

	IN0	E,(SCR)
Ms5:	RES	3,E
	OUT0	(SCR),E		; ROM active
	LD	A,(HL)		; Get 1 byte
	SET	3,E
	OUT0	(SCR),E		; RAM on, now
	LD	(HL),A		; Copy out
	ADD	A,D
	LD	D,A		; Accumulate checksum
	CPI			; HL++, BC--, set P/V bit
	JP	PE,Ms5		; Loop until BC=0 (PE -> BC=0)

	POP	IX		; Save DE (stacked above it)
	POP	AF		; Original ROM checksum
	PUSH	IX		;   Put DE Back
	CP	D		; Match?
	JR	Z,Ms6A
	IN0	A,(SCR)
	RES	3,A		; No: we must have the original ROM
	OUT0	(SCR),A
Ms6A:	POP	DE		; Return RAM bounds in DE

Ms6:	EX	AF,AF'		; Get the original BBR
	OUT0	(BBR),A		; Be sure we're in the original stack!
	RET

MEMRSZE	 EQU   $-MemRtn		; Length to move into RAM

;	.DEPHASE	; Normal addressing, now
;
;  Again, ZDS 3.68 does not recognize the PHASE/DEPHASE pseudo-op.
; Named memory segments are used to get around the problem, and the
; Linker is used to copy the resulting code block into the main ROM
; code area for the EEPROM write.
;
;  At this point, the RamCheck segment is compiled AND STORED at 8200H.
; The linker will copy the segment from 8200h down to 0B00h, using as
; much space as needed for the entire segment.
;
;  The remainder of the MainRom segment must be far enough down in memory
; to allow the Linker to insert the RamCheck segment without overwriting
; the MainRom segment code.  To accomplish this, return to the MainRom
; segment, and advance the program counter far enough to provide adequate
; room for the copy.  In this case, the relocated RamCheck segment is 
; MEMRSZE # of bytes, so set the new ORG to be the current location ($)
; plus the MEMRSZE segment length.

	segment	MainRom
	org	$+MEMRSZE

	SUBTITLE	"Command Extensions"
	NEWPAGE
;======================================================================
;      D E B U G  -  C O M M A N D   E X T E N S I O N S
;======================================================================
; Skeleton for DEBUG extension functions
;   These will typically include system bootstrap, and
; (eventually) Flash-ROM programming functions.

; On entry, HL is a "function" code:

;   If non-zero, it points at the command key-letter in 
; the input line. Subfunctions should identify their letter,
; and branch forward if no match. Finally, return with CY set
; if error.
;   If HL=0, each sub-function should display its help message
; before passing control to its successor. The last should
; clear CY and return.

ExCom:	JR	Flash		; Try the flash commands

;======================================================================
; FLASH-ROM PROGRAMMING (not yet implemented)

; Functions:	1. Move ROM code to high RAM
;		2. Burn ROM from RAM data

Flash:				; Nothing defined, yet
	JR	SysBoot		; Try for OS bootstrap

;======================================================================
; SYSTEM BOOTSTRAP FUNCTION

;  When this command is executed, the memory is set up thus:
;	0000-7FFF	ROM
;	8000-FFFF	RAM

;   This means we cannot load the boot sector at low RAM, so we load it
; at 8000H. It will then (typically) load CP/M at high RAM, before
; killing the ROM and jumping to the BIOS start-point.

;   As a safety feature, the boot-sector is sumchecked for original-style
; system loads.  Once it is successfully read, the checksum is verified, to
; be sure it really is boot code, before jumping to it.  This code extends
; the process with a system which simply uses the boot sector to pass data,
; validating a few selected bytes as an integrity check.
 
BOOTAD	 EQU   8000H		; Boot-load point

SysBoot: LD	A,H
	OR	L		; HL null?
	JR	NZ,TBoot
	LD	HL,btHelp
	CALL	PutMsg		; Yes: display the help
	LD	HL,0
	JP	TConfg		; .To successor

TBoot:	LD	A,(HL)
	CP	'Z'		; Was it a boot command?
	JP	NZ,TConfg	; .No: leave it to the next Comnd

	XOR	A
	LD	(OsEx),A	; Ensure "Return" flag is clear

	CALL	GetNum		; Try for drive
	LD	A,E
	CP	1		;- Boot Only Floppy Drive?
	JP	Z,FdBoot	;-
	CP	2		;- Boot Only SCSI?
	JR	NZ,TBoot0 	;- ..jump if Not
	CALL	CkSCSI	 	;- Else check for presence
	LD	A,(SCSIOk)	;;
	OR	A		;; Do we have a SCSI Controller?
	CALL	Z,BtSCSI	;;   Try Boot if Yes
	JR	No	 	;- ..else return Error

TBoot0:	CP	3		;- Boot Only GIDE?
	JR	NZ,TBoot1 	;- ..jump if Not

	ld	b,2
TBoot0Lp1:
	push	bc
	ld	bc,0		; Boot timeout counter
TBoot0Lp2:
	push	bc
	call	InGIDE	 	;- Else Check presence and Initialize
	pop	bc
	ld	a,(PGMok)	;-
	or	a		;- Do we have a GIDE Master?
	call	z,BtGIDE 	;-  Try to boot if it exists
	dec	bc
	ld	a,b
	or	c
	jr	nz,TBoot0Lp2	; Loop if timeout not done
	pop	bc
	djnz	TBoot0Lp1

	jr	No	 	;- ..else return Error

TBoot1:	xor	a
	ld	(OsEx),a
	LD	A,9		;; Nine tries to boot before going to Debug
	call	BtLoop
No:	SCF			; Invalid...
	RET

btHelp:	DB	"Z                 Boot First Available Device", cr
	DB	"  Z 1, Z 2, or Z 3  Boot from Floppy 0, SCSI 0, or GIDE Master", cr, 0

;======================================================================
; SETUP (Configure) NON-VOLATILE RAM FUNCTION

TConfg:	LD	A,H		;-
	OR	L		;- Null = Help?
	JR	NZ,TConf0	;-
	LD	HL,cfHelp	;-
	CALL	PutMsg		;- Yes: Display Help
	LD	HL,0		;-
	JR	TDTim		;-

TConf0:	LD	A,(HL)		;-
	CP	'S'		;- Setup Command?
	JR	NZ,TDTim	;-  No: Leave to Next Comnd
	JP	TCfg		;- Yes: Configure Clock RAM

cfHelp:	DB	"S                 Setup DS-1302 RAM Parameters", cr, 0

;======================================================================
; Display/Set DS-1302 Date/Time

TDTim:	LD	A,H		;-
	OR	L		;- Help?
	JR	NZ,TDTim0	;- ..jump if Not
	LD	HL,dtHelp	;-
	CALL	PutMsg		;- Yes: Display Help
	LD	HL,0		;-
	JR	LoadRet		;- Continue to next command

TDTim0:	LD	A,(HL)		;-
	CP	'T'		;- Show/Set Date/Time?
	JR	NZ,LoadRet	;-  No: Leave to Next Command
	JP	DTim		;- Yes: Work the Clock

dtHelp:	DB	"T                 Display Date/Time", cr
	DB	"  T S               Set Date/Time", cr,0

;======================================================================
; ROMv5.0 Mods							    TAG
;
; New Command - Load and Return.
; Load the OS from the System Tracks, then return to the Debugger.
LoadRet:
	ld	a,h		; See if doing Help display
	or	l
	jr	nz,SetLd	; No, try loading OS
	ld	hl,LrHelp	; Yes - Display help message
	call	PutMsg
	ld	hl,0		; Flag for next command Help message
	jp	EntP112Basic	;  Do next command help

SetLd:	ld	a,(hl)
	cp	'L'
	jp	nz,EntP112Basic
	ld	a,1
	ld	(OsEx),a	; Set flag for 'Load and Return'
	call	GetNum		; Drive type appended?
	ld	a,e
	cp	1		; Assume Floppy Load
	jp	z,FdBoot	;  and go if 'L 1' entered
	cp	2		; Assume SCSI load
	jr	nz,LBoot0	;  and go if 'L 2' not entered
	call	CkSCSI		; See if SCSI present
	jp	z,BtSCSI	; Load the OS if so
BadLd:	ld	hl,BadMsg	; No OS was loaded, so display the
	call	PutMsg		;  error and exit to the debugger
	jp	ComOk
LBoot0:	cp	3		; Was 'L 3' entered ?
	jp	nz,BAM		; Go if not 'L 3' for GIDE

	ld	b,2
LBoot0Lp1:
	push	bc
	ld	bc,0		; Boot timeout counter
LBoot0Lp2:
	push	bc
	call	InGIDE	 	;- Else Check presence and Initialize
	pop	bc
	ld	a,(PGMok)	;-
	or	a		; Do we have a GIDE Master?
	jp	z,BtGIDE 	; If OK, load GIDE OS
	dec	bc
	ld	a,b
	or	c
	jr	nz,LBoot0Lp2	; Loop if timeout not done
	pop	bc
	djnz	LBoot0Lp1
	jr	BadLd		; else, flag the error and exit
				;  else, fall through...
; The OS loaders will return here if 'Load and Return' was executed.
; HL contains the OS Execution Address.
LrEnt:
	ld	de,LdAdd	; Put ASCII OS execute address here
	call	PutHL		; Convert HL to ASCII string at (DE)
	ld	hl,LdMsg	; Point to msg
	call	PutMsg		;  and display
	jp	DbgLin		; Re-enter the debugger

LrHelp:	db	"L                 Load OS (First Available) and"
	db	" return to DEBUG", cr
	db	"  L 1, L 2, or L 3  Load from Floppy 0, SCSI 0, or GIDE Master", cr, 0

LdMsg:	db	bell, "Boot Track(s) loaded @ 8100H.  Execute via G "
LdAdd:	db	"xxxx ", cr,0
BadMsg:	db	bell, "Nothing Loaded", cr, 0

;======================================================================
; ROMv5.2 Mods							    TAG
;
; New Command - P: Execute P112 Tiny Basic.
; Jump to the Tiny Baisc interpreter in ROM.
EntP112Basic:
	ld	a,h		; See if doing Help display
	or	L
	jr	nz,EnterBasic	; No, enter P112 Tiny Basic
	ld	hl,BasicHelp	; Yes - Display help message
	call	PutMsg
	ld	hl,0		; Flag for next command Help message
	jp	NxtCmd		;  Do next command help

EnterBasic:
	ld	a,(hl)
	cp	'P'
	jr	nz,NxtCmd
	call	GetCh		; Skip Spaces, return next char/EOL
	jp	z,BasicSTART	; ..Cold Start Tiny Basic if No Arg
	cp	'R'		; ReStart Tiny Basic?
	jp	z,BasicReSTART	; ..jump if Yes
	cp	'r'
	jp	z,BasicReSTART	; ..jump if "r" also
	or	0ffh		; Else Set Error Return
	ret

ExitBasic:
	jp	DbgLin

BasicHelp:
	db	"P                 Execute P112 Tiny Basic", cr
	db	"  P R               Re-Execute Tiny Basic", cr, 0

;======================================================================
; Next Function (actually the end)

NxtCmd:	LD	A,H		; Finish: just a HELP ?
	OR	L
	RET	Z		; If so, return success

	scf			; .Else: fail (no-one took it)
	ret

	INCLUDE	"DISKOP.S"
	INCLUDE	"SCSI.S"	;; SCSI Routines
	INCLUDE	"GIDE.S"	;- GIDE (IDE/ATA) Routines
	INCLUDE	"CLKRAM.S"	;- DS-1302 Clock RAM Routines
	INCLUDE	"DATTIM.S"	;- DS-1302 Clock Read/Set Routines
	INCLUDE "DEBUG.S"	; Debug code
	  if  TinyBasic
	INCLUDE	"P112 Tiny Basic.asm"
	  endif

	END
